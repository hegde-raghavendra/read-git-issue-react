import React, { useEffect, useState } from 'react';
import MaterialTable from 'material-table';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container'



const IssueDisplay = props => {

    const [gitIsues, setGitIssues] = useState([]);

    const useStyles = makeStyles({
        table: {
            marginTop: '10ch',
            width: '100ch',
        },
    });

    useEffect(() => {

            let url = new URL('https://gitlab.com/api/v4/projects/15145190/issues');

            if (props.labelToSearch !== '') {
                url.searchParams.append('labels', props.labelToSearch);
            }
            if (props.titleToSearch !== '') {
                url.searchParams.append('search', props.titleToSearch);
                url.searchParams.append('in', 'description');
            }

            console.log(url);

            const myHeaders = new Headers();

            myHeaders.append('PRIVATE-TOKEN', process.env.REACT_APP_GIT_API_SECRET_CODE);

            const myRequest = new Request(url, {
                method: 'GET',
                headers: myHeaders
            });


            fetch(myRequest)
                .then(response => {
                    if (!response.ok) {
                        throw new Error('Failed to fetch.');
                    }
                    return response.json();
                })
                .then(charData => {
                    console.log(charData);
                    setGitIssues(charData);

                })
                .catch(err => {
                    console.log(err);
                });

    }, [props.labelToSearch, props.titleToSearch]);

    const classes = useStyles();

    return (
        <div>

            <br/>

            <Container maxWidth="xl">


                <MaterialTable
                    className={classes.table}
                    title="Git Issues"
                    columns={[
                        { title: 'Title', field: 'title' },
                        {
                            title: 'Url', field: 'web_url',
                            render: rowData => <a href={rowData.web_url} target="_blank" rel="noopener noreferrer"> Link </a>
                        },
                        { title: 'State', field: 'state' },
                        {
                            title: 'Created on', field: 'created_at',
                            render: rowData => rowData.created_at.substring(0, 10)
                        }
                    ]}
                    data={gitIsues}
                    options={{
                        filtering: true,
                        headerStyle: {
                            backgroundColor: '#01579b',
                            color: '#FFF'
                        },
                        rowStyle: {
                            backgroundColor: '#EEE',
                        },
                    }}
                />
            </Container>

        </div>
    );
};

export default IssueDisplay;