import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { TextField } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container'
import IssueDisplay from './IssueDisplay'


const InputForm = props => {

    const [inLabel, setInLabel] = useState(props.labelToSearch);
    const [inTitle, setInTitle] = useState(props.titleToSearch);
    const [showIssueTable, setShowIssueTable] = useState(false);


    const useStyles = makeStyles((theme) => ({
        root: {
            marginTop: '10ch',
            display: 'flex',
            flexWrap: 'wrap',
        },
        textField: {
            marginLeft: theme.spacing(1),
            marginRight: theme.spacing(1),
            marginTop: theme.spacing(2),
            width: '50ch',
        },
        button: {
            marginLeft: theme.spacing(1),
            marginRight: theme.spacing(1),
            marginTop: theme.spacing(2),
            width: '15ch',
        },
    }));


    const handleSubmit = (event) => {
        event.preventDefault();
        console.log(inLabel);
        console.log(inTitle);
        setShowIssueTable(true);

        props.onFormSubmit(inLabel, inTitle);

    }

    const classes = useStyles();

    return (
        <div className={classes.root}>

                <form onSubmit={handleSubmit}>
                    <Container maxWidth="lg">
                        <TextField
                            className={classes.textField}
                            id="inLabel"
                            label="Label to search"
                            variant="outlined"
                            value={inLabel}
                            onChange={e => setInLabel(e.target.value)}
                        >
                        </TextField>
                    </Container>
                    <Container maxWidth="lg">

                        <TextField className={classes.textField}
                            id="inTitle"
                            label="Description to search"
                            variant="outlined"
                            value={inTitle}
                            onChange={e => setInTitle(e.target.value)}
                        >

                        </TextField>
                    </Container>
                    <Container maxWidth="lg">

                        <Button variant="contained" color="primary" size="large" type="submit" className={classes.button}>
                            Submit
                    </Button>
                    </Container>
                </form>

            {showIssueTable && (
                <IssueDisplay
                    labelToSearch={props.labelToSearch}
                    titleToSearch={props.titleToSearch}
                />)}


        </div>
    );
};

export default InputForm;