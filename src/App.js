import React, { useState } from 'react';
import './App.css';
import InputForm from './Components/InputForm'

import AppBarComponent  from "./Components/AppBarComponent";

const App = props => {

  const [labelToSearch, setLabelToSearch] = useState('');
  const [titleToSearch, setTitleToSearch] = useState('');

  const onFormSubmit = (label,title) => {    
    setLabelToSearch(label);
    setTitleToSearch(title);
  }

  return (
    <div className="App">
      <AppBarComponent />
      <InputForm
        labelToSearch={labelToSearch}
        titleToSearch={titleToSearch}
        onFormSubmit={onFormSubmit}
      />
      
    </div>
  );
}

export default App;
